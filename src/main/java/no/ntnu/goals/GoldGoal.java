package no.ntnu.goals;

import no.ntnu.model.Player;

/**
 * The type Gold goal.
 */
public class GoldGoal implements Goal{
  private int minimumGold;

  /**
   * Instantiates a new Gold goal.
   *
   * @param minimumGold the minimum gold
   */
  public GoldGoal(int minimumGold){
    this.minimumGold = minimumGold;
  }

  /**
   * Implements method from Goal
   *
   * @param player
   */
  public boolean isFulfilled(Player player){
    return (player.getGold() >= minimumGold);
  }
}
