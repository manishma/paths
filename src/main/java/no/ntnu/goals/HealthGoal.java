package no.ntnu.goals;

import no.ntnu.model.Player;

/**
 * The type Health goal.
 */
public class HealthGoal implements Goal{
  private int minimumHealth;

  /**
   * Instantiates a new Health goal.
   *
   * @param minimumHealth the minimum health
   */
  public HealthGoal(int minimumHealth){
    this.minimumHealth = minimumHealth;
  }

  /**
   * Implements method from Goal
   *
   * @param player
   */
  public boolean isFulfilled(Player player){
    return (player.getHealth() >= minimumHealth);
  }
}
