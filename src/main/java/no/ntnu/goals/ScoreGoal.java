package no.ntnu.goals;

import no.ntnu.model.Player;

/**
 * The type Score goal.
 */
public class ScoreGoal implements Goal{
  private int minimumPoints;

  /**
   * Instantiates a new Score goal.
   *
   * @param minimumPoints the minimum points
   */
  public ScoreGoal(int minimumPoints){
    this.minimumPoints = minimumPoints;
  }

  /**
   * Implements method from Goal
   *
   * @param player
   */
  public boolean isFulfilled(Player player){
    return (player.getScore() >= minimumPoints);
  }
}
