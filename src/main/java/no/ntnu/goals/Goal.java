package no.ntnu.goals;

import no.ntnu.model.Player;

/**
 * The interface Goal.
 */
public interface Goal {
  /**
   * Is fulfilled boolean.
   *
   * @param player the player
   * @return the boolean
   */
  public boolean isFulfilled(Player player);
}
