package no.ntnu.goals;
import java.util.List;
import no.ntnu.model.Player;

/**
 * The type Inventory goal.
 */
public class InventoryGoal implements Goal{
  private List<String> mandatoryItems;

  /**
   * Instantiates a new Inventory goal.
   *
   * @param mandatoryItems the mandatory items
   */
  public InventoryGoal(List<String> mandatoryItems){
    this.mandatoryItems = mandatoryItems;
  }

  /**
   * Implements method from Goal
   *
   * @param player
   */
  public boolean isFulfilled(Player player){
    return player.getInventory().containsAll(mandatoryItems);
  }
}
