package no.ntnu.model;

import no.ntnu.goals.Goal;

import java.util.List;

/**
 * The class Game.
 */
public class Game {
  private Player player;
  private Story story;
  private List<Goal> goals;

  /**
   * Instantiates a new Game.
   *
   * @param player the player
   * @param story  the story
   * @param goals  the goals
   * @throws IllegalArgumentException the illegal argument exception
   */
  public Game(Player player, Story story, List<Goal> goals) throws IllegalArgumentException{
    if (player == null) throw new IllegalArgumentException("Player cannot be null.");
    if (story == null) throw new IllegalArgumentException("Story cannot be null.");
    if (goals == null) throw new IllegalArgumentException("Goals cannot be null.");

    this.player = player;
    this.story = story;
    this.goals = goals;
  }

  /**
   * Get player player.
   *
   * @return the player
   */
  public Player getPlayer(){
    return this.player;
  }

  /**
   * Get story story.
   *
   * @return the story
   */
  public Story getStory(){
    return this.story;
  }

  /**
   * Get goals list.
   *
   * @return the list
   */
  public List<Goal> getGoals(){
    return this.goals;
  }

  /**
   * Begin passage.
   *
   * @return the passage
   */
  public Passage begin(){
    return this.story.getOpeningPassage();
  }

  /**
   * Go passage.
   *
   * @param link the link
   * @return the passage
   */
  public Passage go(Link link){
    return this.story.getPassage(link);
  }
}
