package no.ntnu.model;

import no.ntnu.actions.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class Link.
 */
public class Link{
  private String text;
  private String reference;
  private List<Action> actions;

  /**
   * Instantiates a new Link.
   *
   * @param text      the text
   * @param reference the reference
   * @throws IllegalArgumentException the illegal argument exception
   */
  public Link(String text, String reference) throws IllegalArgumentException{
    if (text == null) throw new IllegalArgumentException("Text cannot be null.");
    if (text.isBlank()) throw new IllegalArgumentException("Text cannot be blank.");
    if (reference == null) throw new IllegalArgumentException("Reference cannot be null.");
    if (reference.isBlank()) throw new IllegalArgumentException("Reference cannot be blank.");

    this.text = text;
    this.reference = reference;
    this.actions = new ArrayList<Action>();
  }

  /**
   * Get text string.
   *
   * @return the string
   */
  public String getText(){
    return text;
  }

  /**
   * Get reference string.
   *
   * @return the string
   */
  public String getReference(){
    return reference;
  }

  /**
   * Add action boolean.
   *
   * @param action the action
   * @return the boolean
   */
  public boolean addAction(Action action) throws NullPointerException{
    Objects.requireNonNull(action);
    return this.actions.add(action);
  }

  /**
   * Get actions list.
   *
   * @return the list
   */
  public List<Action> getActions(){
    return this.actions;
  }

  /**
   * toString Override.
   *
   * @return Obj outputted as String in given format
   */
  @Override
  public String toString(){
    return "Text: " + text + "\n"
        + "Reference: " + reference + "\n";
  }

  /**
   * equals Override
   *
   * @return boolean
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null || this.getClass() != obj.getClass()) return false;
    Link link = (Link) obj;
    return Objects.equals(this.text, link.text) &&
        Objects.equals(this.reference, link.reference) &&
        Objects.equals(this.actions, link.actions);
  }

  /**
   * hashCode Override
   *
   * @return int
   */
  @Override
  public int hashCode(){
    return Objects.hash(this.text, this.reference);
  }
}
