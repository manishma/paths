package no.ntnu.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class Passage.
 */
public class Passage {
  private String title;
  private String content;
  private List<Link> links;

  /**
   * Instantiates a new Passage.
   *
   * @param title   the title
   * @param content the content
   * @throws IllegalArgumentException the illegal argument exception
   */
  public Passage(String title, String content) throws IllegalArgumentException{
    if (title == null) throw new IllegalArgumentException("Title cannot be null.");
    if (content == null) throw new IllegalArgumentException("Content cannot be null.");
    if (title.isBlank()) throw new IllegalArgumentException("Title cannot be blank.");
    if (content.isBlank()) throw new IllegalArgumentException("Content cannot be blank.");

    this.title = title;
    this.content = content;
    this.links = new ArrayList<Link>();
  }

  /**
   * Get title string.
   *
   * @return the string
   */
  public String getTitle(){
    return this.title;
  }

  /**
   * Get content string.
   *
   * @return the string
   */
  public String getContent(){
    return this.content;
  }

  /**
   * Add link boolean.
   *
   * @param link the link
   * @return the boolean
   */
  public boolean addLink(Link link) throws NullPointerException{
    Objects.requireNonNull(link);
    this.links.add(link);
    return this.links.contains(link);
  }

  /**
   * Get links list.
   *
   * @return the list
   */
  public List<Link> getLinks(){
    return this.links;
  }

  /**
   * Has links boolean.
   *
   * @return the boolean
   */
  public boolean hasLinks(){
    return (this.links.size() > 0);
  }

  /**
   * toString Override.
   *
   * @return Obj outputted as String in given format
   */
  @Override
  public String toString(){
    return "Title: " + title + "\n"
        + "Content: " + content;
  }

  /**
   * equals Override
   *
   * @return boolean
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null || getClass() != obj.getClass()) return false;

    Passage passage = (Passage) obj;

    if (!title.equals(passage.title)) return false;
    if (!content.equals(passage.content)) return false;
    return links.equals(passage.links);

  }

  /**
   * hashCode Override
   *
   * @return int
   */
  @Override
  public int hashCode(){
    return Objects.hash(this.title, this.content, this.links);
  }
}
