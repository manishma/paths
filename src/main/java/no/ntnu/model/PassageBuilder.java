package no.ntnu.model;

import java.util.ArrayList;
import java.util.List;

public class PassageBuilder {
  private String title;
  private String content;
  private List<Link> links;

  public PassageBuilder() {
    this.links = new ArrayList<>();
  }

  public PassageBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  public PassageBuilder setContent(String content) {
    this.content = content;
    return this;
  }

  public PassageBuilder addLink(Link link) {
    this.links.add(link);
    return this;
  }

  public PassageBuilder addAllLinks(List<Link> links) {
    this.links.addAll(links);
    return this;
  }

  public Passage build() {
    Passage passage = new Passage(title, content);
    for (Link link : links) {
      passage.addLink(link);
    }
    return passage;
  }
}
