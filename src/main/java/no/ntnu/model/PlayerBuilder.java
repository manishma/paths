package no.ntnu.model;

public class PlayerBuilder {
  private String name;
  private int health;
  private int score;
  private int gold;

  public PlayerBuilder setName(String name) {
    this.name = name;
    return this;
  }

  public PlayerBuilder setHealth(int health) {
    this.health = health;
    return this;
  }

  public PlayerBuilder setScore(int score) {
    this.score = score;
    return this;
  }

  public PlayerBuilder setGold(int gold) {
    this.gold = gold;
    return this;
  }

  public Player build() {
    return new Player(name, health, score, gold);
  }
}
