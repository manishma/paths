package no.ntnu.model;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.List;
import java.util.stream.Collectors;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class Story.
 */
public class Story {
  private String title;
  private Passage openingPassage;
  private Map<Link, Passage> passages;

  /**
   * Instantiates a new Story.
   *
   * @param title         the title
   * @param openinPassage the openin passage
   * @throws IllegalArgumentException the illegal argument exception
   */
  public Story(String title, Passage openinPassage) throws IllegalArgumentException{
    if (title == null) throw new IllegalArgumentException("Title cannot be null.");
    if (title.isBlank()) throw new IllegalArgumentException("Title cannot be blank.");
    if (openingPassage == null) throw new IllegalArgumentException("Opening passage cannot be null.");

    this.title = title;
    this.openingPassage = openinPassage;
    this.passages = new HashMap<Link, Passage>();
  }

  /**
   * Get title string.
   *
   * @return the string
   */
  public String getTitle(){
    return this.title;
  }

  /**
   * Get opening passage passage.
   *
   * @return the passage
   */
  public Passage getOpeningPassage(){
    return this.openingPassage;
  }

  /**
   * Add passage.
   *
   * @param passage the passage
   */
  public void addPassage(Passage passage){
    Objects.requireNonNull(passage);
    this.passages.put(new Link(passage.getTitle(), passage.getTitle()), passage);
  }


  /**
   * Get passage passage.
   *
   * @param link the link
   * @return the passage
   */
  public Passage getPassage(Link link){
    return this.passages.get(link);
  }

  /**
   * Get passages collection.
   *
   * @return the collection
   */
  public Collection<Passage> getPassages(){
    return this.passages.values();
  }

  /**
   * Removes the passage associated with the provided link. If the passage is referenced by
   * other links, it will not be removed and an IllegalStateException will be thrown.
   *
   * @param link The link associated with the passage to be removed
   * @throws IllegalArgumentException If the provided link does not correspond to any passage
   * @throws IllegalStateException If the passage to be removed is referenced by other links
   */
  public void removePassage(Link link) {
    Passage passageToRemove = passages.get(link);

    if (passageToRemove == null) {
      throw new IllegalArgumentException("The provided link does not correspond to any passage.");
    }

    boolean isReferenced = passages.entrySet().stream()
        .anyMatch(entry -> entry.getValue().equals(passageToRemove) && !entry.getKey().equals(link));

    if (!isReferenced) {
      passages.remove(link);
    } else {
      throw new IllegalStateException("Cannot remove the passage as it is referenced by other links.");
    }
  }

  /**
   * Returns a list of links in the passages map that no longer have an associated passage.
   *
   * @return A list of broken links, i.e., links without an associated passage
   */
  public List<Link> getBrokenLinks() {
    return passages.entrySet().stream()
        .filter(entry -> entry.getValue() == null)
        .map(Map.Entry::getKey)
        .collect(Collectors.toList());
  }

  public void saveStoryToFile(String fileName) throws IOException, URISyntaxException {
    URI resourceURI = getClass().getResource("/").toURI();
    Path resourcesPath = Paths.get(resourceURI);
    Path filePath = resourcesPath.resolve(fileName + ".paths");

    try (BufferedWriter writer = Files.newBufferedWriter(filePath)) {
      for (Passage passage : passages.values()) {
        writer.write("::" + passage.getTitle() + "\n");
        writer.write(passage.getContent() + "\n");
        for (Link link : passage.getLinks()) {
          writer.write("[" + link.getText() + "](" + link.getReference() + ")\n");
        }
        writer.write("\n");
      }
    }
  }

  public static Story loadStoryFromFile(String fileName) throws IOException, URISyntaxException {
    URI resourceURI = Story.class.getResource("/" + fileName + ".paths").toURI();
    Path filePath = Paths.get(resourceURI);

    Pattern titlePattern = Pattern.compile("^::(.+)$");
    Pattern linkPattern = Pattern.compile("^\\[(.+)]\\((.+)\\)$");

    Passage openingPassage = null;
    Map<Link, Passage> loadedPassages = new HashMap<>();

    try (BufferedReader reader = Files.newBufferedReader(filePath)) {
      String line;
      Passage currentPassage = null;

      while ((line = reader.readLine()) != null) {
        Matcher titleMatcher = titlePattern.matcher(line);
        Matcher linkMatcher = linkPattern.matcher(line);

        if (titleMatcher.find()) {
          String title = titleMatcher.group(1);
          String content = reader.readLine();
          currentPassage = new Passage(title, content);

          if (openingPassage == null) {
            openingPassage = currentPassage;
          }
        } else if (linkMatcher.find() && currentPassage != null) {
          String linkText = linkMatcher.group(1);
          String linkReference = linkMatcher.group(2);
          Link link = new Link(linkText, linkReference);
          currentPassage.addLink(link);

          // Store passage by its link for later
          loadedPassages.put(link, currentPassage);
        }
      }
    }

    if (openingPassage == null) {
      throw new IllegalStateException("No opening passage found in the file.");
    }

    Story loadedStory = new Story(openingPassage.getTitle(), openingPassage);
    loadedStory.passages = loadedPassages;
    return loadedStory;
  }
}
