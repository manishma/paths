package no.ntnu.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The class Player.
 */
public class Player {
  private String name;
  private int health;
  private int score;
  private int gold;
  private List<String> inventory;

  /**
   * Instantiates a new Player.
   *
   * @param name   the name
   * @param health the health
   * @param score  the score
   * @param gold   the gold
   * @throws IllegalArgumentException the illegal argument exception
   */
  public Player(String name, int health, int score, int gold) throws IllegalArgumentException {
    if (name == null) throw new IllegalArgumentException("Name cannot be null.");
    if (name.isBlank()) throw new IllegalArgumentException("Name cannot be blank.");
    if (health < 0) throw new IllegalArgumentException("Health cannot be negative.");
    if (score < 0) throw new IllegalArgumentException("Score cannot be negative.");
    if (gold < 0) throw new IllegalArgumentException("Gold cannot be negative.");
    if (inventory == null) throw new IllegalArgumentException("Inventory cannot be null.");

    this.name = name;
    this.health = health;
    this.score = score;
    this.gold = gold;
    this.inventory = new ArrayList<String>();
  }


  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Add health.
   *
   * @param health the health
   */
  public void addHealth(int health){
    this.health += health;
  }

  /**
   * Gets health.
   *
   * @return the health
   */
  public int getHealth() {
    return this.health;
  }

  /**
   * Gets score.
   *
   * @return the score
   */
  public int getScore() {
    return this.score;
  }

  /**
   * Add score.
   *
   * @param points the points
   */
  public void addScore(int points){
    this.score += points;
  }

  /**
   * Gets gold.
   *
   * @return the gold
   */
  public int getGold() {
    return this.gold;
  }

  /**
   * Add gold.
   *
   * @param gold the gold
   */
  public void addGold(int gold){
    this.gold += gold;
  }

  /**
   * Add to inventory.
   *
   * @param item the item
   */
  public void addToInventory(String item){
    this.inventory.add(item);
  }

  /**
   * Get inventory list.
   *
   * @return the list
   */
  public List<String> getInventory(){
    return this.inventory;
  }
}
