package no.ntnu.actions;

import no.ntnu.model.Player;

/**
 * The type Inventory action.
 */
public class InventoryAction implements Action{
  private String item;

  /**
   * Instantiates a new Inventory action.
   *
   * @param item the item
   */
  public InventoryAction(String item){
    this.item = item;
  }

  /**
   * Implements method from Action
   *
   * @param player
   */
  public void execute(Player player){
    player.addToInventory(item);
  }
}
