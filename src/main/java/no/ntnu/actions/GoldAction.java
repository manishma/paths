package no.ntnu.actions;

import no.ntnu.model.Player;

/**
 * The type Gold action.
 */
public class GoldAction implements Action{
  private int gold;

  /**
   * Instantiates a new Gold action.
   *
   * @param gold the gold
   */
  public GoldAction(int gold){
    this.gold = gold;
  }

  /**
   * Implements method from Action
   *
   * @param player
   */
  public void execute(Player player){
    player.addGold(gold);
  }
}
