package no.ntnu.actions;

import no.ntnu.model.Player;

/**
 * The type Score action.
 */
public class ScoreAction implements Action{
  private int points;

  /**
   * Instantiates a new Score action.
   *
   * @param points the points
   */
  public ScoreAction(int points){
    this.points = points;
  }

  /**
   * Implements method from Action
   *
   * @param player
   */
  public void execute(Player player){
    player.addScore(points);
  }
}
