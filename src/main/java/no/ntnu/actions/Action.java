package no.ntnu.actions;

import no.ntnu.model.Player;

/**
 * The interface Action.
 */
public interface Action {
  /**
   * Execute.
   *
   * @param player the player
   */
  public void execute(Player player);
}
