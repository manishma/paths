package no.ntnu.actions;

import no.ntnu.model.Player;

/**
 * The type Health action.
 */
public class HealthAction implements Action{
  private int health;

  /**
   * Instantiates a new Health action.
   *
   * @param health the health
   */
  public HealthAction(int health){
    this.health = health;
  }

  /**
   * Implements method from Action
   *
   * @param player
   */
  public void execute(Player player){
    player.addHealth(health);
  }
}
