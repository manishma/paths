package no.ntnu;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;


/**
 * JavaFX App
 */
public class App extends Application {

    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage stage) {
        VBox root = new VBox();

        HBox hbox1 = new HBox();
        hbox1.setPrefHeight(100);

        AnchorPane anchorPane1 = new AnchorPane();
        anchorPane1.setPrefHeight(200);
        anchorPane1.setPrefWidth(200);

        Button saveLoadButton = new Button("Save and Load");
        saveLoadButton.setLayoutX(14);
        saveLoadButton.setLayoutY(14);

        Button addGoalsButton = new Button("Add Goals");
        addGoalsButton.setLayoutX(14);
        addGoalsButton.setLayoutY(50);

        anchorPane1.getChildren().addAll(saveLoadButton, addGoalsButton);

        Region region1 = new Region();
        region1.setPrefHeight(200);
        region1.setPrefWidth(200);

        AnchorPane anchorPane2 = new AnchorPane();
        anchorPane2.setPrefHeight(200);
        anchorPane2.setPrefWidth(200);

        Label tipsLabel = new Label("Tips");
        tipsLabel.setLayoutX(86);
        tipsLabel.setLayoutY(6);

        Label tip1Label = new Label("1. ...");
        tip1Label.setLayoutX(83);
        tip1Label.setLayoutY(33);

        anchorPane2.getChildren().addAll(tipsLabel, tip1Label);

        hbox1.getChildren().addAll(anchorPane1, region1, anchorPane2);

        HBox hbox2 = new HBox();
        hbox2.setPrefHeight(160);
        hbox2.setPrefWidth(600);

        Region region2 = new Region();
        region2.setPrefHeight(192);
        region2.setPrefWidth(453);

        AnchorPane anchorPane3 = new AnchorPane();
        anchorPane3.setPrefHeight(192);
        anchorPane3.setPrefWidth(134);

        Button optionsButton = new Button("Options");
        optionsButton.setLayoutX(134);
        optionsButton.setLayoutY(8);

        Button actionsButton = new Button("Actions");
        actionsButton.setLayoutX(134);
        actionsButton.setLayoutY(42);

        Label actionLabel = new Label("1. ...");
        actionLabel.setLayoutX(86);
        actionLabel.setLayoutY(88);

        anchorPane3.getChildren().addAll(optionsButton, actionsButton, actionLabel);

        hbox2.getChildren().addAll(region2, anchorPane3);

        HBox hbox3 = new HBox();
        hbox3.setPrefHeight(137);
        hbox3.setPrefWidth(600);

        AnchorPane anchorPane4 = new AnchorPane();
        anchorPane4.setPrefHeight(137);
        anchorPane4.setPrefWidth(134);

        Label playerInfoLabel = new Label("PlayerInfo");
        playerInfoLabel.setLayoutX(14);
        playerInfoLabel.setLayoutY(14);

        Label healthLabel = new Label("Health:");
        healthLabel.setLayoutX(14);
        healthLabel.setLayoutY(42);

        Label pointsLabel = new Label("Points:");
        pointsLabel.setLayoutX(14);
        pointsLabel.setLayoutY(60);

        anchorPane4.getChildren().addAll(playerInfoLabel, healthLabel, pointsLabel);

        Region region3 = new Region();
        region3.setPrefHeight(137);
        region3.setPrefWidth(453);

        AnchorPane anchorPane5 = new AnchorPane();
        anchorPane5.setPrefHeight(137);
        anchorPane5.setPrefWidth(134);

        Label opponentInfoLabel = new Label("OpponentInfo");
        opponentInfoLabel.setLayoutX(14);
        opponentInfoLabel.setLayoutY(14);

        Label opponentHealthLabel = new Label("Health:");
        opponentHealthLabel.setLayoutX(14);
        opponentHealthLabel.setLayoutY(42);

        Label opponentPointsLabel = new Label("Points:");
        opponentPointsLabel.setLayoutX(14);
        opponentPointsLabel.setLayoutY(60);

        anchorPane5.getChildren().addAll(opponentInfoLabel, opponentHealthLabel, opponentPointsLabel);

        hbox3.getChildren().addAll(anchorPane4, region3, anchorPane5);

        root.getChildren().addAll(hbox1, hbox2, hbox3);

        Scene scene = new Scene(root, 600, 400);

        String cssPath = getClass().getResource("/no/ntnu/layout.css").toExternalForm();
        scene.getStylesheets().add(cssPath);

        stage.setScene(scene);
        stage.setTitle("Game");
        stage.show();
    }
}