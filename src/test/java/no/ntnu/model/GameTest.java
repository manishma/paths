package no.ntnu.model;

import no.ntnu.goals.Goal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
  Game game;

  @BeforeEach
  void init() {
    Player player = new Player("Name", 1, 1, 1);
    Story story = new Story("Title", new Passage("Title", "Content"));
    List<Goal> goals = new ArrayList<>();
    game = new Game(player, story, goals);
  }

  @Test
  void testParameters() {
    assertEquals("Name", game.getPlayer().getName());
    assertEquals("Title", game.getStory().getTitle());
  }

  @Test
  void testParametersFail() {
    assertThrows(IllegalArgumentException.class, () -> new Game(null, new Story("Title", new Passage("Title", "Content")), new ArrayList<>()),
        "Player cannot be null.");

    assertThrows(IllegalArgumentException.class, () -> new Game(new Player("Name", 1, 1, 1), null, new ArrayList<>()),
        "Story cannot be null.");

    assertThrows(IllegalArgumentException.class, () -> new Game(new Player("Name", 1, 1, 1), new Story("Title", new Passage("Title", "Content")), null),
        "Goals cannot be null.");
  }

  @Test
  void testBegin() {
    Passage openingPassage = game.begin();
    assertEquals("Title", openingPassage.getTitle());
  }

  @Test
  void testGo() {
    game.getStory().addPassage(new Passage("Next Passage", "Content for next passage"));
    Link link = new Link("Next Passage", "Next Passage");
    Passage nextPassage = game.go(link);
    assertEquals("Next Passage", nextPassage.getTitle());
  }
}
