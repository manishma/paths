package no.ntnu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class StoryTest {
  Story story;

  @BeforeEach
  void init() {
    story = new Story("Title", new Passage("Title", "Content"));
  }

  @Test
  void testParameters() {
    assertEquals("Title", story.getTitle());
    assertEquals("Title", story.getOpeningPassage().getTitle());
    assertEquals("Content", story.getOpeningPassage().getContent());
    Passage newPassage = new Passage("Another Title", "Another Content");
    story.addPassage(newPassage);
    assertEquals(1, story.getPassages().size());
    assertTrue(story.getPassages().contains(newPassage));
  }

  @Test
  void testParametersFail() {
    try {
      Story newStory = new Story("", new Passage("Title", "Content"));
      fail("Method did not throw IllegalArgumentException as expected");
    } catch (IllegalArgumentException ex) {
      assertEquals(ex.getMessage(), "Title cannot be blank.");
    }

    try {
      Story newStory = new Story(null, new Passage("Title", "Content"));
      fail("Method did not throw IllegalArgumentException as expected");
    } catch (IllegalArgumentException ex) {
      assertEquals(ex.getMessage(), "Title cannot be null.");
    }

    try {
      Story newStory = new Story("Title", null);
      fail("Method did not throw IllegalArgumentException as expected");
    } catch (IllegalArgumentException ex) {
      assertEquals(ex.getMessage(), "Opening passage cannot be null.");
    }
  }

  @Test
  void testRemovePassage() {
    Passage passage1 = new Passage("Passage 1", "Content");
    story.addPassage(passage1);

    assertEquals(1, story.getPassages().size());

    story.removePassage(new Link("Passage 1", "Passage 1"));

    assertEquals(0, story.getPassages().size());
  }

  @Test
  void testGetBrokenLinks() {
    Passage passage1 = new Passage("Passage 1", "Content");
    story.addPassage(passage1);

    List<Link> brokenLinks = story.getBrokenLinks();

    assertTrue(brokenLinks.isEmpty());

    story.removePassage(new Link("Passage 1", "Passage 1"));

    brokenLinks = story.getBrokenLinks();

    assertEquals(1, brokenLinks.size());
  }

  @Test
  void testSaveAndLoadStory() {
    Passage openingPassage = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
    Story originalStory = new Story("Example Story", openingPassage);
    Passage anotherRoom = new Passage("Another room", "The door opens to another room. You see a desk with a large, dusty book.");
    originalStory.addPassage(anotherRoom);
    openingPassage.addLink(new Link("Try to open the door", "Another room"));

    String fileName = "testStory";
    File savedFile = new File("src/main/resources/" + fileName);

    // Save the story to a file

    try {
      originalStory.saveStoryToFile(fileName);
    } catch (IOException | URISyntaxException e) {
      fail("Saving the story failed: " + e.getMessage());
    }

    // Load the story from the saved file
    Story loadedStory;
    try {
      loadedStory = Story.loadStoryFromFile(fileName);
    } catch (IOException | URISyntaxException e) {
      fail("Loading the story failed: " + e.getMessage());
      return;
    }

    // Clean up the saved file
    try {
      Files.delete(Path.of(savedFile.getPath()));
    } catch (IOException e) {
      System.err.println("Failed to delete the saved file: " + e.getMessage());
    }

    // Compare the original story and the loaded story
    assertEquals(originalStory.getTitle(), loadedStory.getTitle());
    assertEquals(originalStory.getOpeningPassage().getTitle(), loadedStory.getOpeningPassage().getTitle());
    assertEquals(originalStory.getOpeningPassage().getContent(), loadedStory.getOpeningPassage().getContent());
    assertEquals(originalStory.getPassages().size(), loadedStory.getPassages().size());

    Passage originalAnotherRoom = originalStory.getPassage(new Link("Another room", "Another room"));
    Passage loadedAnotherRoom = loadedStory.getPassage(new Link("Another room", "Another room"));
    assertNotNull(loadedAnotherRoom);
    assertEquals(originalAnotherRoom.getTitle(), loadedAnotherRoom.getTitle());
    assertEquals(originalAnotherRoom.getContent(), loadedAnotherRoom.getContent());
  }
}
