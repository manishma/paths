package no.ntnu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PassageTest {
  private Passage passage;
  private Link link1;
  private Link link2;

  @BeforeEach
  void setUp() {
    link1 = new Link("Link1", "Target1");
    link2 = new Link("Link2", "Target2");

    passage = new PassageBuilder()
        .setTitle("Title")
        .setContent("Content")
        .addLink(link1)
        .addLink(link2)
        .build();
  }

  @Test
  void getTitle() {
    assertEquals("Title", passage.getTitle());
  }

  @Test
  void getContent() {
    assertEquals("Content", passage.getContent());
  }

  @Test
  void addLink() {
    Link link3 = new Link("Link3", "Target3");
    assertTrue(passage.addLink(link3));
    assertTrue(passage.getLinks().contains(link3));
  }

  @Test
  void getLinks() {
    List<Link> links = passage.getLinks();
    assertEquals(2, links.size());
    assertTrue(links.contains(link1));
    assertTrue(links.contains(link2));
  }

  @Test
  void hasLinks() {
    assertTrue(passage.hasLinks());
  }

  @Test
  void testToString() {
    String expectedString = "Title: Title\nContent: Content";
    assertEquals(expectedString, passage.toString());
  }

  @Test
  void testEquals() {
    Passage otherPassage = new PassageBuilder()
        .setTitle("Title")
        .setContent("Content")
        .addLink(link1)
        .addLink(link2)
        .build();

    assertTrue(passage.equals(otherPassage));
  }

  @Test
  void testHashCode() {
    Passage otherPassage = new PassageBuilder()
        .setTitle("Title")
        .setContent("Content")
        .addLink(link1)
        .addLink(link2)
        .build();

    assertEquals(passage.hashCode(), otherPassage.hashCode());
  }
}
