package no.ntnu.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
  Player player;

  @BeforeEach
  void init() {
    player = new PlayerBuilder()
        .setName("Name")
        .setHealth(1)
        .setScore(1)
        .setGold(1)
        .build();
  }

  @Test
  void testParameters() {
    assertEquals("Name", player.getName());
    assertEquals(1, player.getHealth());
    assertEquals(1, player.getScore());
    assertEquals(1, player.getGold());
  }

  @Test
  void testParametersFail() {
    assertThrows(IllegalArgumentException.class, () -> new PlayerBuilder()
            .setName("")
            .setHealth(1)
            .setScore(1)
            .setGold(1)
            .build(),
        "Name cannot be blank.");

    assertThrows(IllegalArgumentException.class, () -> new PlayerBuilder()
            .setName("Name")
            .setHealth(-1)
            .setScore(1)
            .setGold(1)
            .build(),
        "Health cannot be negative.");

    assertThrows(IllegalArgumentException.class, () -> new PlayerBuilder()
            .setName("Name")
            .setHealth(1)
            .setScore(-1)
            .setGold(1)
            .build(),
        "Score cannot be negative.");

    assertThrows(IllegalArgumentException.class, () -> new PlayerBuilder()
            .setName("Name")
            .setHealth(1)
            .setScore(1)
            .setGold(-1)
            .build(),
        "Gold cannot be negative.");
  }

  @Test
  void testInventory() {
    player.addToInventory("Item 1");
    assertEquals(1, player.getInventory().size());
    assertTrue(player.getInventory().contains("Item 1"));
  }

  @Test
  void testAddHealth() {
    player.addHealth(2);
    assertEquals(3, player.getHealth());
  }

  @Test
  void testAddScore() {
    player.addScore(3);
    assertEquals(4, player.getScore());
  }

  @Test
  void testAddGold() {
    player.addGold(5);
    assertEquals(6, player.getGold());
  }
}
