package no.ntnu.model;

import no.ntnu.actions.InventoryAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkTest {
  private Link link;

  @BeforeEach
  void init() {
    link = new Link("Link", "Reference");
  }

  @Test
  void testParameters() {
    assertEquals("Link", link.getText());
    assertEquals("Reference", link.getReference());
    link.addAction(new InventoryAction("InventoryAction"));
    assertEquals(link.getActions().size(), 1);
  }

  @Test
  void testParametersFail() {
    assertThrows(IllegalArgumentException.class, () -> new Link("", "Reference"), "Text cannot be blank.");
    assertThrows(IllegalArgumentException.class, () -> new Link("Link", ""), "Reference cannot be blank.");
  }

  @Test
  void testToString() {
    String expected = "Text: Link\nReference: Reference\n";
    assertEquals(expected, link.toString());
  }

  @Test
  void testEqualsAndHashCode() {
    Link link2 = new Link("Link", "Reference");
    assertTrue(link.equals(link2) && link2.equals(link));
    assertEquals(link.hashCode(), link2.hashCode());

    Link link3 = new Link("Different", "Reference");
    assertFalse(link.equals(link3));
  }
}
